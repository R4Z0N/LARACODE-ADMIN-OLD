<?php

namespace App\Http\Controllers\Laracode;

use App\Http\Controllers\Controller;
use App\Http\Requests\UserRequest;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use Yajra\DataTables\Facades\DataTables;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (request()->ajax())
            return DataTables::eloquent(
                User::query()->with(['roles', 'permissions']))
                    ->addColumn('action', function($user) {
                           return view('lc-admin.users.datatables', compact('user'));
                       })
                    ->make(true);

        return view('lc-admin.users.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Role::get();
        $permissions = Permission::get();

        return view('lc-admin.users.create', compact('roles', 'permissions'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserRequest $request)
    {

        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
        ]);
        $roles = array_flatten(
            Role::whereIn('id', [$request->roles])
                ->get(['name'])
                ->toArray()
            );

        $permissions = array_flatten(
            Permission::whereIn('id', [$request->permissions])
                ->get(['name'])
                ->toArray()
            );

        activity()
            ->performedOn($user)
            ->causedBy(Auth::user())
            ->withProperties([
                'attributes' => [
                    'roles' => implode(', ', $roles)
                ]
            ])
            ->log('Synced roles');

        activity()
            ->performedOn($user)
            ->causedBy(Auth::user())
            ->withProperties([
                'attributes' => [
                    'permissions' => implode(', ', $permissions)
                ]
            ])
            ->log('Synced permissions');

        $user->syncRoles($request->roles);
        $user->syncPermissions($request->permissions);

        switch ($request->action) {

            case 'save':
                return redirect()->route('lc-admin.users.edit', ['user' => $user->id]);
            
            case 'save-and-new':
                return redirect()->route('lc-admin.users.create');

            case 'save-and-back':
                return redirect()->route('lc-admin.users.index');
        }
    }

    public function edit(User $user) {

        $roles = Role::get();
        $permissions = Permission::get();
        return view('admin.users.edit', compact('roles', 'permissions', 'user'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('lc-admin.users.profile');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        $roles = array_flatten(
            Role::whereIn('id', [$request->roles])
                ->get(['name'])
                ->toArray()
            );

        $permissions = array_flatten(
            Permission::whereIn('id', [$request->permissions])
                ->get(['name'])
                ->toArray()
            );

        // activity()
        //     ->performedOn($user)
        //     ->causedBy(Auth::user())
        //     ->withProperties([
        //         'attributes' => '{' . implode(', ', $roles) . '}',
        //         'old' => '{' . implode(', ', $user->roles->pluck('name')->toArray()) . '}'
        //     ])
        //     ->log('Synced roles');

        // activity()
        //     ->performedOn($user)
        //     ->causedBy(Auth::user())
        //     ->withProperties([
        //         'attributes' => '{' . implode(', ', $permissions) . '}',
        //         'old' => '{' . implode(', ', $user->permissions->pluck('name')->toArray()) . '}'
        //     ])
        //     ->log('Synced permissions');

        $user->syncRoles($request->roles);
        $user->syncPermissions($request->permissions);

        $user->update([
            'name' => $request->name,
            'email' => $request->email,
        ]);
        switch ($request->action) {

            case 'save':
                return redirect()->route('lc-admin.users.edit', ['user' => $user->id]);
            
            case 'save-and-new':
                return redirect()->route('lc-admin.users.create');

            case 'save-and-back':
                return redirect()->route('lc-admin.users.index');
        }
    }

    public function updateMany(Request $request)
    {
        $users = User::findMany($request->ids);

        $users->syncRoles($request->roles);
        $users->syncPermissions($request->permissions);

        return 200;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        User::destroy($id);

        return 200;
    }
}
