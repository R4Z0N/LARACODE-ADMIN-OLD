<?php

namespace App\Laracode;

use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{
    protected $guarded = [];
}
