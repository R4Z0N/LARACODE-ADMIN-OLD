<?php

namespace App\Laracode;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Models\Activity as SpatieActivity;

class Activity extends SpatieActivity
{
    public function getNewAttributesAttribute()
    {
        if (isset($this->properties['attributes']))
            return $this->properties['attributes'];

        return null;
    }

    public function getOldAttributesAttribute()
    {
        if (isset($this->properties['old']))
            return $this->properties['old'];

        return null;
    }
}
