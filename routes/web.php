<?php

Auth::routes();
Route::get('/', 'HomeController@index');
Route::get('home', 'HomeController@index');

Route::get('lc-admin', 'Laracode\AdminController@index')->name('lc-admin');
Route::group(['as' => 'lc-admin.', 'prefix' => 'lc-admin', 'middleware' => ['auth', 'permission:admin panel'], 'namespace' => 'Laracode'], function () {

    Route::get('dashboard', 'AdminController@dashboard')->name('dashboard');
    // Route::resource('roles', 'RoleController', ['except' => ['create', 'show', 'edit']]);
    Route::resource('roles', 'RoleController', ['except' => ['show', 'edit']]);
    // Route::resource('permissions', 'PermissionController', ['except' => ['create', 'show', 'edit']]);
    Route::resource('permissions', 'PermissionController', ['except' => ['show', 'edit']]);
    Route::resource('users', 'UserController');

    Route::get('languages', 'LanguageController@index')->name('languages');
    Route::post('languages/{name}', 'LanguageController@store')->name('store-language');
    Route::get('set-language/{language}', 'LanguageController@update')->name('set-language');

    Route::put('users', 'UserController@updateMany'); // obrisati?
    Route::get('activity-log', 'AdminController@activityLog')->name('activity-log');

    Route::post('newsletter', 'NewsletterController@store')->name('store-newsletter');
});



