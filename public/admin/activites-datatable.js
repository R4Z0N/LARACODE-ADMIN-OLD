jQuery(document).ready(function () {
    datatable = $('#table-activity-log').DataTable({
                responsive: true, 
                searchDelay: 500, 
                processing: true, 
                serverSide: true, 
                ajax:"/lc-admin/activity-log", 
                columns: [
                    { data: "description" }, 
                    { data: "subject.name" }, 
                    { data: "causer.name" },
                    { data: "created_at" }
                ]
            });

    function format (d) {
        // `d` is the original data object for the row
        return d.attributes;
    }

    $('#table-activity-log').on('click', 'td:not(:last-child)', function () {
        var tr = $(this).parent();
        var row = datatable.row(tr);
 
        if ( row.child.isShown() ) {
            // This row is already open - close it
            $('div.slider', row.child()).slideUp(function () {
                row.child.hide();
                tr.removeClass('shown');
            });
        }
        else {
            // Open this row
            row.child( format(row.data()), 'no-padding' ).show();
            tr.addClass('shown');
             
            $('div.slider', row.child()).slideDown();;
        }
    });


});
