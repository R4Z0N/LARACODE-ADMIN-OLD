<div class="slider">
    <table>
        @foreach(array_keys($activity->newAttributes) as $key)
            <tr>
                <td>{{ $key }}</td>
                @if(isset($activity->oldAttributes[$key]))
                    <td>
                        <span class="datatable-old-value">
                            {{ $activity->oldAttributes[$key] }}
                        </span>  
                    </td>
                    <td>
                        =>
                    </td>
                @endif
                <td>
                    <span class="datatable-new-value">
                        {{ $activity->newAttributes[$key] }}
                    </span>
                </td>
            </tr>
        @endforeach
    </table>
</div>
