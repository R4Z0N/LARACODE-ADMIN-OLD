<nav class="breadcrumb ks-default">
	@foreach ($breadcrumbs as $breadcrumb)	
	@if($breadcrumb->url && $breadcrumb->title == 'Home')
    <a class="breadcrumb-item ks-breadcrumb-icon" href="{{ $breadcrumb->url }}">
        <span class="la la-home ks-icon"></span>
    </a>
    @elseif($breadcrumb->url)
    <a href="{{ $breadcrumb->url }}" class="breadcrumb-item">{{ $breadcrumb->title }}</a>
    @elseif($breadcrumb->url && $loop->last)
    <span class="breadcrumb-item active" href="{{ $breadcrumb->url }}">{{ $breadcrumb->title }}</span>
    @endif
    @endforeach
</nav>