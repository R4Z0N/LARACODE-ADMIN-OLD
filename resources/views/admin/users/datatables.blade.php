<form action="{{ route('lc-admin.users.destroy', [$user->id]) }}" method="POST" class="float-left">
   {{method_field('DELETE')}}
   @csrf
   <button type="submit" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="Drop"><i class="la la-remove"></i></button>
</form>
<a href="{{ route('lc-admin.users.edit', ['users' => $user->id]) }}" class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" title="View"><i class="la la-edit"></i></a>
