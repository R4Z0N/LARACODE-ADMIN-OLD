@extends('admin.layouts.master')
@section('title', 'Activity log')
@section('external-css')
<!--begin::Page Vendors Styles -->
<link href="{{ asset('METRONIC/assets/vendors/custom/datatables/datatables.bundle.css')}}" rel="stylesheet" type="text/css" /><!--RTL version:<link href="../../../assets/vendors/custom/datatables/datatables.bundle.rtl.css" rel="stylesheet" type="text/css" />-->
@endsection
@section('content')
<style>
.shown+tr {
  background: #f8f9fa !important;
}
.shown+tr .datatable-old-value {
  color: red;
}
.shown+tr .datatable-new-value {
  color: green;
}  
div.slider {
  display: none;
}
table.dataTable tbody td.no-padding {
  padding: 0;
}
</style>
<div class="m-grid__item m-grid__item--fluid m-wrapper">
  <!-- BEGIN: Subheader -->
  <div class="m-subheader ">
   <div class="d-flex align-items-center">
{{ Breadcrumbs::render('lc-admin.activity-log') }}
</div>
</div>
<!-- END: Subheader -->             
<div class="m-content">
  <div class="m-portlet m-portlet--mobile">
    <div class="m-portlet__head">
      <div class="m-portlet__head-caption">
        <div class="m-portlet__head-title">
          <h3 class="m-portlet__head-text">
            {{ __('Activity log') }}
          </h3>
        </div>
      </div>
      <div class="m-portlet__head-tools">
        <a href="" class="btn btn-danger m-btn m-btn--icon m-btn--sm m--margin-right-10">
          <span>
            <i class="la la-remove"></i>
            <span>Clear activity log</span>
          </span>
        </a>
      </div>
    </div>
    <div class="m-portlet__body">
      <!--begin: Datatable -->
      <div class="m-portlet__body">
        <!--begin: Datatable -->
        <table class="table table-striped- table-bordered table-hover table-checkable" id="table-activity-log">
          <thead>
            <tr>
              <th>{{ __('Description') }}</th>
              <th>{{ __('Subject') }}</th>
              <th>{{ __('Causer') }}</th>
              <th>{{ __('Created at') }}</th>
            </tr>
          </thead>
        </table>
      </div>

    </div>
  </div>
  <!-- END EXAMPLE TABLE PORTLET-->               </div>
</div>



@endsection
@section('external-js')

<script src="{{ asset('js/datatables.bundle.js') }}"></script>
<script src="{{ asset('admin/activites-datatable.js') }}"></script>

@endsection