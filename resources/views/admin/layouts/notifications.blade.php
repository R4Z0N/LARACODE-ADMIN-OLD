<script type="text/javascript">

	{{-- returned notification --}}
	@if (session()->has('notification'))

		$.notify(
			{!! session('notification') !!}
		);

	@endif

	{{-- automatically fired notification on error --}}
	@if ($errors->isNotEmpty())

		$.notify({
			icon: 'glyphicon glyphicon-warning-sign',
			title: 'Validation error!',
			message: 'Please pay attention to fields marked in red!',
		}, {
			type: 'danger'
		});

	@endif

	{{-- ajax notification --}}
	function ajaxNotify(notification) {
		var options 	= JSON.parse(notification)[0];
		var settings 	= JSON.parse(notification)[1];
		
		$.notify(options, settings);
	}
	
</script>