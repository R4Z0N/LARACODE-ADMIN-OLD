<!-- BEGIN: Aside Menu -->
<div id="m_ver_menu" class="m-aside-menu  m-aside-menu--skin-dark m-aside-menu--submenu-skin-dark " m-menu-vertical="1" m-menu-scrollable="1" m-menu-dropdown-timeout="500" style="position: relative;">
    <ul class="m-menu__nav  m-menu__nav--dropdown-submenu-arrow ">
        <li class="m-menu__item  {{ strpos(Route::currentRouteName(), 'lc-admin.dashboard') === 0 ? 'm-menu__item--active' : null }}" aria-haspopup="true"><a href="{{ route('lc-admin.dashboard') }}" class="m-menu__link "><i class="m-menu__link-icon flaticon-line-graph"></i><span class="m-menu__link-title">  <span class="m-menu__link-wrap">      <span class="m-menu__link-text">{{ __('Dashboard') }}</span>  </span></span></a></li>
        <li class="m-menu__section ">
            <h4 class="m-menu__section-text">Generals</h4>
            <i class="m-menu__section-icon flaticon-more-v2"></i>
        </li>
        
        <li class="m-menu__item  m-menu__item--submenu {{ strpos(Route::currentRouteName(), 'lc-admin.users') === 0 || strpos(Route::currentRouteName(), 'lc-admin.roles') === 0 || strpos(Route::currentRouteName(), 'lc-admin.permissions') === 0 ? 'm-menu__item--expanded m-menu__item--open' : null }}" aria-haspopup="true"><a href="javascript:;" class="m-menu__link m-menu__toggle"><i class="m-menu__link-icon flaticon-users"></i><span class="m-menu__link-text">{{ __('Users') }}</span><i class="m-menu__ver-arrow la la-angle-right"></i></a>
            <div class="m-menu__submenu "><span class="m-menu__arrow"></span>
                <ul class="m-menu__subnav">
                    <li class="m-menu__item {{ strpos(Route::currentRouteName(), 'lc-admin.users') === 0 ? 'm-menu__item--active' : null }}" aria-haspopup="true"><a href="{{ route('lc-admin.users.index') }}" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text">{{ __('Users') }}</span></a></li>
                    <li class="m-menu__item {{ strpos(Route::currentRouteName(), 'lc-admin.roles') === 0 ? 'm-menu__item--active' : null }}" aria-haspopup="true"><a href="{{ route('lc-admin.roles.index') }}" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text">{{ __('Roles') }}</span></a></li>
                    <li class="m-menu__item {{ strpos(Route::currentRouteName(), 'lc-admin.permissions') === 0 ? 'm-menu__item--active' : null }}" aria-haspopup="true"><a href="{{ route('lc-admin.permissions.index') }}" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text">{{ __('Permissions') }}</span></a></li>
                </ul>
            </div>
        </li>
        <li class="m-menu__item  {{ strpos(Route::currentRouteName(), 'lc-admin.activity-log') === 0 ? 'm-menu__item--active' : null }}" aria-haspopup="true"><a href="{{ route('lc-admin.activity-log') }}" class="m-menu__link "><i class="m-menu__link-icon la la-file-archive-o"></i><span class="m-menu__link-title">  <span class="m-menu__link-wrap">      <span class="m-menu__link-text">{{ __('Activity log') }}</span>  </span></span></a></li>
        <li class="m-menu__item  {{ strpos(Route::currentRouteName(), 'lc-admin.languages') === 0 ? 'm-menu__item--active' : null }}" aria-haspopup="true"><a href="{{ route('lc-admin.languages') }}" class="m-menu__link "><i class="m-menu__link-icon la la-file-archive-o"></i><span class="m-menu__link-title">  <span class="m-menu__link-wrap">      <span class="m-menu__link-text">{{ __('Languages') }}</span>  </span></span></a></li>
    </ul>
</div>
<!-- END: Aside Menu -->